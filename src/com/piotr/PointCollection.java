package com.piotr;

import java.util.HashSet;

public class PointCollection extends HashSet<PointCollection.Point> {

    //this saves the instantiation of a point in mem every time contains is called
    private Point searchPoint = new Point(0,0);

    public PointCollection(int initialCount) {
        super(initialCount);
    }

    //make more efficient

    public Point remove(int x, int y) {
        Point soughtPoint = new Point(x, y);
        if(super.remove(soughtPoint)) {
            return soughtPoint;
        } else {
            return null;
        }
    }

    //make more efficient
    public boolean contains(int x, int y) {
        searchPoint.x = (byte) x;
        searchPoint.y = (byte) y;
        return super.contains(searchPoint);
    }


    public static class Point {
        private byte x;
        private byte y;

        public Point(byte x, byte y) {
            this.x = x;
            this.y = y;
        }

        public Point(int x, int y) {
            this((byte)x, (byte)y);
        }


        public byte getX() {
            return x;
        }

        public byte getY() {
            return y;
        }

        @Override
        public int hashCode() {
            return getX();
        }

        @Override
        public boolean equals(Object o) {

            if(o == this) return true;
            if(!(o instanceof Point)) return false;
            return getX() == ((Point) o).getX() && getY() == ((Point) o).getY();
        }

        @Override
        public String toString() {
            return "[" + x + "," + y + ']';
        }
    }
}
