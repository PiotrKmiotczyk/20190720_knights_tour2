package com.piotr;

import java.util.*;

public class Utils {

    public static String concat(Collection<Character> src) {
        StringBuilder sb = new StringBuilder();
        return concatRaw(src, sb).toString();
    }

    private static StringBuilder concatRaw(Collection<Character> src, StringBuilder sb) {
        for (Character character : src) {
            sb.append(character);
        }
        return sb;
    }

//    public static String concat2D(Collection<Collection<Character>> src) {    //todo: now this not working is mystifying...
    public static <T> String concat2D(Collection<HashSet<Character>> src) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        Iterator<HashSet<Character>> iterator = src.iterator();
        while(iterator.hasNext()) {
            Collection<Character> innerSrc = iterator.next();

            concatRaw(innerSrc, sb);

            if(iterator.hasNext()) {
                sb.append(", ");
            }
        }

        sb.append("]");
        return sb.toString();
    }
}
