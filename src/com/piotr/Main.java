package com.piotr;

import java.util.*;

/**
 * You can later use the CLI visualiser.py to show results.
 */
public class Main {


    public static void main(String[] args) {
	    // write your code here
        PointCollection.Point startingPos = new PointCollection.Point(1, 1);
        PointCollection.Point boardSize = new PointCollection.Point(6, 6);
        new Game(startingPos, boardSize).run();
    }


}


class Game {

//    Logger logger = Logger.getLogger(Game.class.getSimpleName());

    private PointCollection.Point startingPos;
    private PointCollection potentialPositions;
    private List<HashSet<Character>> potentialMoves;
    private LinkedList<Character> movesHistory;
    private LinkedList<PointCollection.Point> positionHistory;
    public static final List<Character> ALL_MOVE_TYPES = Arrays.asList('W', 'w', 'S', 's', 'A', 'a', 'D', 'd');


    public Game(PointCollection.Point startingPos, PointCollection.Point boardSize) {
        this.startingPos = startingPos;

        potentialPositions = prepareAvailableFields(boardSize, startingPos);
        movesHistory = new LinkedList<>();
        potentialMoves = initPotentialMoves(boardSize);
        positionHistory = new LinkedList<>();
    }

    private List<HashSet<Character>> initPotentialMoves(PointCollection.Point boardSize) {
        int count = boardSize.getX() * boardSize.getY() - 1;
        List<HashSet<Character>> allMoves = new ArrayList<>(count);
        for(int i=0; i<count; i++) {
            allMoves.add(new HashSet<>(ALL_MOVE_TYPES));
        }
        return allMoves;
    }


    public void run() {
        long debugFreqCounter = 0;
        positionHistory.add(startingPos);

        int moveNo = 0;

        //the first condition is the win condition
        //the second is the condition of continuation (zero potential moves at root level means we've tried all paths)
        while(!potentialPositions.isEmpty() && !potentialMoves.get(0).isEmpty()) {
            try {

                makeMove(positionHistory.getLast(), moveNo);
                moveNo++;

//                System.out.println(Utils.concat(movesHistory));

            } catch (DeadEndException e) {

                //undo move
                //remove last used potential move from previous level
                if(!movesHistory.isEmpty()) {
                    movesHistory.removeLast();
                }
                //but restore all potential moves on this level (because previous one being different, this will be a different path)
                if(moveNo>0) {
                    potentialMoves.set(moveNo, new HashSet<>(ALL_MOVE_TYPES));
                    moveNo--;
                    PointCollection.Point lastPosition = positionHistory.removeLast();
                    potentialPositions.add(lastPosition);
                }

//                System.out.println("Dead end (moveNo: " + moveNo + ")." );
            }

            //print path progress
//            System.out.println(Utils.concat(movesHistory));
            if(debugFreqCounter++ >= 10000000) {
                debugFreqCounter = 0;
                System.out.println(Utils.concat2D(potentialMoves));
            }
        }

        //summary:
        if(potentialPositions.isEmpty()) {
            System.out.println("Board filled: " + Utils.concat(movesHistory));

        } else if (potentialMoves.get(0).isEmpty()) {
            System.out.println("Out of possible paths, could not fill board: " + Utils.concat(movesHistory));
        }

//        System.out.println(movesHistory);
    }



    private void makeMove(PointCollection.Point lastPos, int moveNo) throws DeadEndException {

        if (tryMove(lastPos, moveNo, 'W', 1, 2)) return;

        if (tryMove(lastPos, moveNo, 'w', -1, 2)) return;

        if (tryMove(lastPos, moveNo, 'D', 2, 1)) return;

        if (tryMove(lastPos, moveNo, 'd', 2, -1)) return;

        if (tryMove(lastPos, moveNo, 'S', + 1, - 2)) return;

        if (tryMove(lastPos, moveNo, 's', - 1, - 2)) return;

        if (tryMove(lastPos, moveNo, 'A', - 2, + 1)) return;

        if (tryMove(lastPos, moveNo, 'a', - 2, - 1)) return;

        //dead end (no free squares in range, or out of board)
        //or all paths at this level have been tried
        throw new DeadEndException("Dead end.");
    }

    private boolean tryMove(PointCollection.Point lastPos, int moveNo, char moveCode, int dx, int dy) {


        if (potentialMoves.get(moveNo).contains(moveCode)) {
            int newX = lastPos.getX() + dx;
            int newY = lastPos.getY() + dy;

            if (potentialPositions.contains(newX, newY)) {

                updateMoveState(newX, newY, moveCode, moveNo);
                return true;

            } else {

                //this is only needed in case moveNo == 0, so can be potentially optimised
                // in case of other moves tha first we are able to backtrack
                // so information about the possibilities we backtrack from is irrelevant
                if(moveNo == 0) {
                    removePotentialMove(moveCode, moveNo);
                }
            }
        }
        return false;
    }


    private PointCollection.Point updateMoveState(int newX, int newY, char moveCode, int moveNo) {
        PointCollection.Point newPos = potentialPositions.remove(newX, newY);
        positionHistory.add(newPos);
        movesHistory.add(moveCode);

        removePotentialMove(moveCode, moveNo);
        return newPos;
    }

    private void removePotentialMove(char moveCode, int moveNo) {
//        System.out.println("Removing move from potential moves (moveNo: " + moveNo + ").");
        potentialMoves.get(moveNo).remove(moveCode);
    }

    private PointCollection prepareAvailableFields(PointCollection.Point boardSize, PointCollection.Point startingPos) {
        int count = boardSize.getX() * boardSize.getY() - 1;

        PointCollection fields = new PointCollection(count);

        for(int x = 1; x <= boardSize.getX(); x++) {;
            for(int y = 1; y <= boardSize.getX(); y++) {

                if(x == startingPos.getX() && y == startingPos.getY()) {
                    continue;
                }
                fields.add(new PointCollection.Point(x, y));
            }
        }

        return fields;
    }
}

