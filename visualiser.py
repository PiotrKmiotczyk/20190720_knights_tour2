import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import re



startPos = [1,1]
pos, allMoves, xdata, ydata = [], [], [], []
boardRng = [1, 5]
gridRng = [boardRng[0] - 0.5, boardRng[1] + 0.5]
gridLns = np.linspace(gridRng[0], gridRng[1], gridRng[0] + gridRng[1])
movesPatternStr, oldMovesPatternStr = '',''
ln, fix, ax = None, None, None

def init():
	global pos
	global allMoves

	pos = list(startPos)
	print("startPos: " + str(startPos))
	allMoves = [tuple(startPos), ]


	global fig, ax
	fig, ax = plt.subplots()

	global xdata, ydata
	xdata, ydata = [], []
	
	
	ax.set_xlim(gridRng)
	ax.set_ylim(gridRng)
	ax.set_xticks(gridLns, minor=False)
	ax.set_yticks(gridLns, minor=False)
	ax.xaxis.grid(True, which='major')
	ax.yaxis.grid(True, which='major')


	#draw empty data set initially
	# blue diamonds ;)
	global ln
	ln, = plt.plot(xdata, ydata, 'bD')

	global movesPatternStr, oldMovesPatternStr
	oldMovesPatternStr = movesPatternStr
	movesPatternStr = ''



def moveInBoardRange(currPos):
	 return (boardRng[0] <= pos[0] <= boardRng[1]
	 	and boardRng[0] <= pos[1] <= boardRng[1])


def moveIsRepeated(currPos):
	# print("check if " + str(allMoves) + " contains " + str(currPos))
	return tuple(currPos) in allMoves


def strIsGood(movesString):
	pattern = re.compile("^([wWdDsSaA]+|exit)$")
	return pattern.match(movesString)


def applyMovesFromString(movesString):
	print("applying moves from starting pos: " + str(pos))

	for char in movesString:
		if char == 'W':		#up right
			pos[0] += 1
			pos[1] += 2
		elif char == 'w':
			pos[0] += -1
			pos[1] += 2
		elif char == 'D':
			pos[0] += 2
			pos[1] += 1
		elif char == 'd':
			pos[0] += 2
			pos[1] += -1
		elif char == 'S':
			pos[0] += 1
			pos[1] += -2
		elif char == 's':
			pos[0] += -1
			pos[1] += -2
		elif char == 'A':
			pos[0] += -2
			pos[1] += 1
		elif char == 'a':
			pos[0] += -2
			pos[1] += -1
		else:
			print("Unwanted char in the string.")

		print(str(pos))

		if not moveInBoardRange(pos):
			print("We jumped off the board!")
			break

		if moveIsRepeated(pos):
			print("We stepped on the same square!")
			break

		allMoves.append(tuple(pos))


	print("Parsed string: " + movesString + " as: " + str(allMoves))



def update(nextCoord):
    label = str(len(xdata))
    xdata.append(nextCoord[0])
    ydata.append(nextCoord[1])
    ln.set_data(xdata, ydata)
    ax.annotate(label, nextCoord)

    return ln,





init()

# while True:

# movesPatternStr = input("Enter move pattern or 'exit': ")
# movesPatternStr = "WWWSSSWwwAsssDWWDSssdww"
# movesPatternStr = "WSADswdw"
# movesPatternStr = "WdaWdaWdaWdaWda"
movesPatternStr = "WWdsaWASdWwasdDwAsddwWaA"

# if not strIsGood(movesPatternStr):
# 	print("There is an error in the pattern.")
	# continue

# if movesPatternStr == 'exit':
# 	break



applyMovesFromString(movesPatternStr)


ani = FuncAnimation(fig, update, frames=allMoves,
                    blit=False, interval=100, repeat=False)
plt.show()	



print("Reinitializing drawing")
plt.clf()
plt.cla()
plt.close()
plt.close('all')

init()
